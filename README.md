# Bitbucket Pipelines Pipe Sample: Azure Static Web Apps Deploy (Node.js)
This sample uses the Bitbucket Azure Web Apps Deploy pipeline to deploy a sample Vanilla API app to [App Service Static Web Apps](https://docs.microsoft.com/en-us/azure/static-web-apps/).

## Prerequisites

- **Active Azure account:** If you don't have one, you can [create an account for free](https://azure.microsoft.com/free/).
- **Bitbucket project:** If you don't have one, you can [create a project for free](https://confluence.atlassian.com/bitbucketserver/creating-projects-776639848.html).
  - Bitbucket includes **Pipelines**. If you need help getting started with Pipelines, see [Create your first pipeline](https://support.atlassian.com/bitbucket-cloud/docs/get-started-with-bitbucket-pipelines/).
  
## Support

This sample is provided "as is" and is not supported. Likewise, no commitments are made as to its longevity or maintenance. To discuss this sample with other users, please visit the Azure DevOps Services section of the Microsoft Developer Community: https://developercommunity.visualstudio.com/spaces/21/index.html.
